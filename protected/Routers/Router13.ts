import * as express from 'express'
import { FoodService13 } from '../Services/Service13'

export class FoodRouter13{
    //private foodService: FoodService13 = new FoodService13
    private foodService13: FoodService13 
    constructor(foodService13:FoodService13){
        this.foodService13 = foodService13
    }
    public router(){
        const router = express.Router()
        router.get('/get', this.getAll)
        router.get('/clear', this.clearAll)
        router.get('/click', this.click)
        

        return router
    }

    private getAll = (req: express.Request, res: express.Response) => {       
        try{
            res.json(req.session.foodPurchase) 
        }catch(e){
            res.redirect('./page12.html')
        }
    } 

    private clearAll = (req:express.Request, res: express.Response) => {
        req.session.foodPurchase = null
        res.json(req.session.foodPurchase) 
    }

    private click = async (req: express.Request, res: express.Response) => {
        this.foodService13.click()
    }
}