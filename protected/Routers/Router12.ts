import * as express from 'express'
import { FoodService12 } from '../Services/Service12'
import { FoodService13 } from '../Services/Service13'
import * as fs  from 'fs';
import * as multer from 'multer';


export class FoodRouter12{
    private foodService12: FoodService12 =  new FoodService12();
    private foodService13: FoodService13 
    // No Use
    private databaseBoss: any
    constructor(foodService13:FoodService13){
        this.foodService13 = foodService13
        
    }
    public router(){

        const storage = multer.diskStorage({
            destination: function (req, file, cb) {
              cb(null, `protected/uploads/`);
            },
            filename: function (req, file, cb) {
              cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
            }
          })
        const upload = multer({storage})

        
        const router = express.Router()


        router.get('/get', this.getAll)
        router.post('/post', this.purchaseDetail)
        router.get('/markDownRecord', this.mark)
        router.post('/update', this.update)
        router.post('/', upload.single('food-pic'), this.uploadFoodImage)
        router.post('/delete-clicked', this.deleteClicked)
        router.post('/add', this.add)
       
        

        return router
    }

    private uploadFoodImage = (req: express.Request, res: express.Response) => {
       
        try{
            
            res.json(req.file)
        }catch(e){
            console.error("errrrrrrrror")
            
        }
    }

    private deleteClicked = (req: express.Request, res: express.Response) => {
        // Do it on client side, not server side, do them in update
        req.session.deleteClicked = req.body
        res.json("ok")// res.json({message:"ok"});
    }

    private getAll = (req: express.Request, res: express.Response) => {
        // Missing Try-catch
        res.json(this.foodService12.getAll())
    }
    private purchaseDetail = (req: express.Request, res: express.Response) => {
        // Do it on client side
        // Shopping Cart :
        // 0. Do everything in JS
        // 1. localStorage:  not good  - not working in incognito mode
        // 2. req.session : not good - not working in incognito mode
        // 3. Store the shopping Cart in JSON(Database)
        try{
            req.session.foodPurchase = {detail: req.body.detail, total: req.body.total}
            
            console.log(req.session.foodPurchase)
            console.log(req.session.passport.user)
            

            res.json('OK')
        }catch(e){
            res.json(e)
        }
    }

    // No Use
    private mark = async (req: express.Request, res: express.Response) => {
        
        try {
            this.databaseBoss = JSON.parse(fs.readFileSync('boss.json', 'utf8'))
        }catch (e){
            this.databaseBoss = []
        }
        this.databaseBoss.push(this.foodService13)
       
        await fs.promises.writeFile('boss.json', JSON.stringify(this.databaseBoss))        
        res.json('OK!')   
    }

    private update = async (req: express.Request, res: express.Response) => {
        try{
            for (let keys in req.session.deleteClicked){
                if (req.session.deleteClicked[keys] != null){
                    req.body[req.session.deleteClicked[keys]].flag = "expired"
                }
            }
            const finalUpdate = req.body.filter(status => status.flag == "current")
            this.foodService12.dataUpdate(finalUpdate)
            res.json('OK!!') // should be a json
            console.log(this.foodService12.food)
        }catch(e){
            console.error(e)
        }
    }

    private add = async (req: express.Request, res: express.Response) => {
        this.foodService12.add(req.body.position, req.body.detail)
        res.json('ok!!!') // should be a json
    }

    
}



