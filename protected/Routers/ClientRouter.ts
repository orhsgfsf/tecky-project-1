
import * as jsonfile from "jsonfile";
import * as express from 'express'
//import { ClientService } from './ClientService'
import * as fs from 'fs';
import * as moment from 'moment'

export interface User {
    id: number,
    username: string,
    password: string,

}

export class ClientRouter {
    // private clientService: ClientService = new ClientService
    private databaseClient: any
    private users: User[]
    private client: any
    private dataForBoss : any
    private totalCost: number = 0
    private bodyForBoss: {}
    private clientHistory: any
    private couponStatus: any
    private keys: any
    private finalCouponData: any
    private getOrder: []
    private money: number
    private delete: {}
    private getOrder2: any
    
    //private clientService: ClientService = new ClientService()


    public router() {
        const router = express.Router()
        router.post('/upload', this.clientUpload)
        // POST /client 
        router.post('/upload-user-data', this.addUserData)
        // POST /user-data
        router.get('/get-user-data', this.get)
        // GET /user-data
        router.get('/getID', this.getID)
        // GET /user/id
        router.get('/clock', this.clock)
        // GET /history
        router.get('/get-history', this.getClientHistory)
        // GET /coupon
        router.get('/get-selected-coupon', this.getSelectedCoupon )
        router.get('/get-coupon-detailFinal', this.getCouponFinalData)
        router.get('/get-food-order', this.getFoodOrder)
        router.get('/get-money', this.getMoney)
        router.post('/upload-selected-coupon', this.uploadSelectedCoupon)
        // POST /coupon
        router.post('/update-coupon-status', this.couponDetail)
        // PUT /coupon-status
        router.post('/delete-ordered', this.deleteOrder)
        // DELETE /order




        return router
    }

    private clientUpload = async (req: express.Request, res: express.Response) => {

        try {
            this.databaseClient = JSON.parse(fs.readFileSync('client.json', 'utf8'))
        } catch (e) {
            this.databaseClient = []
        }
        this.databaseClient.push(req.body.data)

        await fs.promises.writeFile('client.json', JSON.stringify(this.databaseClient))
        res.json('OK!')
    }

    private addUserData = async (req: express.Request, res: express.Response) => {

        this.users = jsonfile.readFileSync(('./User.json'))
        console.log(this.users)
        const findUser = await this.users.find(user => user.id === req.session.passport.user)

        if (findUser) {
            try {
                this.client = jsonfile.readFileSync((`./User${req.session.passport.user}.json`))                
            } catch (e) {
                this.client = []
            }

            try{
                this.dataForBoss = jsonfile.readFileSync(`./purchase-for-boss.json`)
            } catch (e) {
                this.dataForBoss = []
            }

            try{
                this.money = jsonfile.readFileSync('./money.json')
            } catch (e) {
                this.money = 0
            }
        }
        this.money += req.body.purchaseTotalAfterCoupon
        this.bodyForBoss += req.body

        this.bodyForBoss = {
            ...this.bodyForBoss = {
                purchaseDetail: req.body.purchaseDetail,                
                purchaseTotalAfterCoupon: req.body.purchaseTotalAfterCoupon,
                sale: req.body.sale,
                purchaseTime: req.body.purchaseTime,
                address: req.body.address,
                flag: "current",
            }
        }
        console.log(this.bodyForBoss)
        console.log(this.dataForBoss.length)
        

        this.dataForBoss.push(this.bodyForBoss)
        this.client.push(req.body)
        await jsonfile.writeFile('./money.json', this.money)
        await jsonfile.writeFile(`./purchase-for-boss.json`, this.dataForBoss)
        await jsonfile.writeFile(`./User${req.session.passport.user}.json`, this.client)
        res.json('OK!')

    }

    private getMoney = (req: express.Request, res: express.Response) => {
        res.json(jsonfile.readFileSync(('./money.json')))
    }

    private getFoodOrder = async(req: express.Request, res: express.Response) => {
        try {
            this.getOrder = jsonfile.readFileSync((`./purchase-for-boss.json`))
        }catch (e){
            this.getOrder = []
        }
        res.json(this.getOrder)

    }

    private get = async (req: express.Request, res: express.Response) => {
        this.users = jsonfile.readFileSync(('./User.json'))
        res.json(this.users)
    }

    private getID = async (req: express.Request, res: express.Response) => {
        res.json(req.session.passport.user)
    }

    // Do it in client side
    private clock = (req: express.Request, res: express.Response) => {
        res.json(moment().format('lll'))
       
    }

    private getClientHistory = async (req: express.Request, res: express.Response) => {
        try{
            this.clientHistory = await jsonfile.readFileSync(`./User${req.session.passport.user}.json`)
            
        }catch(e){
            this.clientHistory = []
        }
        res.json(this.clientHistory)
    }

    // If it only updates session , don't come to server!
    private uploadSelectedCoupon = async (req: express.Request, res: express.Response) => {
        req.session.couponSelected = req.body
        console.log(req.session.couponSelected)
        console.log('etffff')
        res.json('okkkkkk')
    }

    private getSelectedCoupon = async (req: express.Request, res: express.Response) => {
        res.json(req.session.couponSelected)
    }

    private couponDetail = async (req: express.Request, res: express.Response) => {
        try{
            this.couponStatus = await jsonfile.readFileSync(`./User${req.session.passport.user}Coupon.json`)
        }catch(e){
            this.couponStatus = []
        }
        
        

        for (let keys in req.session.couponSelected){
            if (req.session.couponSelected[keys] != null){
                console.log(req.session.couponSelected[keys])
                this.couponStatus[req.session.couponSelected[keys]].flag = "expired"
            }            
        }

        const couponFilter = this.couponStatus.filter(status => status.flag == "current")

        couponFilter.push(req.body)

        if (couponFilter.length > 5){
           
            for (let i = 0; i < couponFilter.length - 5; i++){
                this.couponStatus[i].flag = "expired"
            }            
        }


        const couponFilter2 = couponFilter.filter(status => status.flag == "current")
        


       
    
        await jsonfile.writeFile(`./User${req.session.passport.user}Coupon.json`, couponFilter2)
        
    }

    private getCouponFinalData = async (req: express.Request, res: express.Response) => {
        this.finalCouponData =  await jsonfile.readFileSync(`./User${req.session.passport.user}Coupon.json`)
        console.log(this.finalCouponData)
        res.json(this.finalCouponData)
       
    }

    private deleteOrder = async (req: express.Request, res: express.Response) => {
        try{

            this.getOrder2 = jsonfile.readFileSync('./purchase-for-boss.json')      
        }catch(e){
            this.getOrder2 = []
        }

        console.log(req.body)
      
        this.getOrder2[req.body.id].flag = "expired" 
        this.delete = this.getOrder2.filter(order => order.flag == "current")
        await jsonfile.writeFile('./purchase-for-boss.json', (this.delete))
        res.json('ok')
        


    }

}