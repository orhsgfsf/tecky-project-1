export class FoodService13{
    private food: object = {}
    private total: number = 0
    private clickk: number = 0
    

    
    addPurchaseDetail = (foodDetail:any) => {
        this.food = foodDetail
    }

    addTotalCost = (cost:number) => {
        this.total = cost
    }

    getPurchaseDetail() {
        return this.food
    }

    getTotal() {
        return this.total
    }

    resetPurchaseDetail() {
        return this.food = {}
    }

    resetTotal() {
        return this.total = 0
    }

    
    click() {
        return 1
    }

    clearClick() {
        return 0
    }

}