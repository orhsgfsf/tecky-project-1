export class FoodService12{
   
    // Should be stored inside JSON file since menu can be updated
    public food:any = [{
        Name: '勁爆牛肉飯',       
        // Id: 1,
        Price: 500,
        remain: 10
        ,
        image: 'uploads/beef-rice.jpg',
        flag: "current"
    },
    {
        Name: '＿春雞飯',        
        // Id: 2,
        Price: 450,
        remain: 15
        ,
        image: 'uploads/chickenrice.jpg',
        flag: "current"
    },
    {
        Name: '極黯然銷魂飯',
     
        // Id: 3,
        Price: 600,
        remain: 10
        ,
        image: 'uploads/chasiurice.jpg',
        flag: "current"
    },
    {
        Name: '崑崙山陽春麵',
     
        // Id: 4,
        Price: 900,
        remain: 10
        ,
        image: 'uploads/3brother.jpg',
        flag: "current"
    },
    {
        Name: '8000度高溫油炸扑扑脆雞',
     
        // Id: 5,
        Price: 1200,
        remain: 20
        ,
        image: 'uploads/chicken.png',
        flag: "current"
    },
    {
        Name: '北海道富士山牛奶',
     
        // Id: 6,
        Price: 1800,
        remain: 20
        ,
        image: 'uploads/milk.jpg',
        flag: "current"
    },
    {
        Name: '＿水',
     
        // Id: 7,
        Price: 9999,
        remain: 5
        ,
        image: 'uploads/water.jpg',
        flag: "current"
    }]

    

    

    
    public getAll(){

        // Add id , should be in add method
        this.food.forEach((o, i) => o.Id = i + 1)
        return this.food
    }
    
    public dataUpdate(update:[]){
        return this.food = update
    }

    public add(order, foodDetail){
        const insert = (arr, index, newItem) => [
            ...arr.slice(0, index),
            newItem,
            ...arr.slice(index)
        ]
        const result = insert(this.food, order - 1, foodDetail)
        return this.food = result
    }
}