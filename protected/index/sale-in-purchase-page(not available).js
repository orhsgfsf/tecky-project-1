class Client {
    constructor(){
        this.coupon = document.querySelector(".coupon")
        this.clientCoupon = {}
        this.coupon9 = []
        this.loadClientData()
    }

    loadClientData = async () => {
        const res = await fetch('/client/get-history')
        this.clientHistory = await res.json()
        
        this.clientHistory.forEach((o,i) => o.purchaseTimes = i+1)
                
        for (let client of this.clientHistory){
                this.clientCoupon[client.purchaseTimes] = {purchaseTotal: client.purchaseTotal, sale: client.sale, purchaseTime: client.purchaseTime}
            }
               
        console.log(this.clientCoupon)

        let buyTimes = Object.keys(this.clientCoupon)
        console.log(buyTimes)

        if (buyTimes.length > 9){
            for (let i = buyTimes.length - 1; i > buyTimes.length - 10; i--){
                this.coupon9.push(buyTimes[i])
            }
    
            buyTimes = this.coupon9.sort((a, b) => {
                return a - b
            })

            console.log(buyTimes)

            
        }


        let html = ""

        buyTimes.map((times) => {
            html += `
                    <div data-id="${times}" class="col-4 coupon-container">
                        <div class="coupon-block">
                            <div>消費日期</div>
                            <div class="text-center">${this.clientCoupon[times].purchaseTime}</div>
                            <div>消費金額</div>
                            <div class="text-center">$${this.clientCoupon[times].purchaseTotal}</div>
                            <div>獲得優惠</div>
                            <div class="text-center">$${this.clientCoupon[times].sale}</div>
                        </div>
                    </div>
                    `
        })

        this.coupon.innerHTML = html

        if (html != ""){
            let statement = document.querySelector(".statement")
            statement.innerHTML = '<h3>使用優惠券：</h3>'
        }
        

        

    }
}

new Client()