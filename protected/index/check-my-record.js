class CheckRecord {
    constructor() {
        this.record = document.querySelector('.record')
        this.history = {}
        this.load()
        this.detailNumber = []
    }
    load = async () => {
        const res = await fetch('/client/get-history')
        this.clientHistory = await res.json()

        this.clientHistory.forEach((o, i) => o.purchaseTimes = i + 1)

        for (let client of this.clientHistory) {
            this.history[client.purchaseTimes] = { detail: client.purchaseDetail, total: client.purchaseTotal, sale: client.sale, time: client.purchaseTime }
        } console.log(this.history)

        const number = Object.keys(this.history)

       
            

        


        console.log(this.detailNumber)

        console.log(number)



        // for (let num of number){            
        //     html += this.renderHistory(num)
        //     console.log(this.history[num].detail)            
        // }
        // this.record.innerHTML = html


        let html2 = ''
        for (let num of number) {

            if (number.length > 0) {
                this.detailNumber = Object.keys(this.history[num].detail)

            } else {
                number.length = 0
            }
            const history = this.renderHistory(num)
            const total = this.renderHistoryCost(num)
            let historyDetail = ''
            for (let detailNum of this.detailNumber) {
               
                if (this.history[num].detail[detailNum].qty != 0) {
                    historyDetail += this.renderHistoryDetail(this.history[num].detail[detailNum])
                }
            }
            html2 += `<div class="row purchase-description"> ${history} ${historyDetail} ${total} </div>`
        }


        this.record.innerHTML = html2

        if (Object.keys(this.history).length > 0) {
            document.querySelector('.check-record-statement').innerHTML = '我的購買紀錄：'
        } else {
            document.querySelector('.check-record-statement').innerHTML = '冇呀，上一頁啦'
        }


        const back = document.querySelector('.back')
        back.addEventListener('click', this.backClick)




    }



    renderHistoryDetail = (history) => {
        return `
       
            <div class="col-6 food statement-center2">
                <div class=food-name"></div>
                <div>               
                    <h3>${history.name}</h3>
                </div>
            </div>
            <div class="col-6 food statement-qty-center">
                <div>
                    <h3>x ${history.qty}</h3>
                </div>
            </div>
        
        `
    }

    renderHistory = (num) => {
        return `
        <div class="col-12 statement-center">     
            <div>
                <h3>購買時間：</h3>
            </div>
            <div>         
                <h3>${this.history[num].time}</h3>
            </div>
        </div>
        `
    }

    renderHistoryCost = (num) => {

        return `
        <div class="col-12 statement-center"> 
            <div>
                <h3>總額：</h3>
            </div>    
            <div>         
            <h3>${this.history[num].total}</h3>
           
            </div>
        </div>
        `
    }


    backClick = () => {
        location.href = './page12.html'
    }
}



let checkMyRecord = new CheckRecord();
