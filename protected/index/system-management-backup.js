class SystemManagement {
    constructor() {

        this.loadFoodMenu()
        this.foodMenuWithID = {}
        this.menu = document.querySelector('.menu')

        this.foodUpdate = []
        this.check = {}
        this.addFood = {}

    }

    loadFoodMenu = async () => {



        const res = await fetch('/foods12/get')
        this.foodMenu = await res.json()

        let html = ''
        for (let foodMenu of this.foodMenu) {
            this.foodMenuWithID[foodMenu.Id] = { name: foodMenu.Name, price: foodMenu.Price, remain: foodMenu.remain, image: foodMenu.image }
            html += this.renderFoodMenu(foodMenu)

        }

        this.menu.innerHTML = html


        const change = document.querySelectorAll('.fade .real-change-button')
        for (let button of change) {
            button.addEventListener('click', this.changeStatus)
        }

        const del = document.querySelectorAll('.delete')
        for (let button of del) {
            button.addEventListener('click', this.deleteButton)
        }

        const add = document.querySelector('button.real-add-button')
        add.addEventListener('click', this.addClick)


        const finalChange = document.querySelector('.final-change')
        finalChange.addEventListener('click', this.finalChange)

        const foodImage = document.querySelectorAll('.upload-food-image')
        for (let button of foodImage) {
            button.addEventListener('submit', async (event) => {

                event.preventDefault()

                const form = document.querySelector(`#modal-${event.target.dataset.id} .upload-food-image`)
                const formData = new FormData(form)

                await fetch('/foods12', {
                    method: 'POST',
                    body: formData

                })

            })
        }

        document.querySelector('.add-food-image').addEventListener('submit', async (event) => {
            event.preventDefault()

            const form2 = document.querySelector('.add-food-image')
            const formData2 = new FormData(form2)

            await fetch('/foods12', {
                method: 'POST',
                body: formData2
            })
        })


    }


    renderFoodMenu = (food) => {
        return `
        <div class="food block-${food.Id}">
        <div class="row">
        <div class="col-12 food-image">
            <img src="${food.image}" class="picture-size"> 
        </div>
        
        
        <div class="col-6 food-name">
            <h3>${food.Id}. ${food.Name}</h3>
        </div>
        <div class="col-6 food-price">
            <h3>($${food.Price})</h3>
        </div> 
        
       
       
        <div class="col-6">
            <h3> 剩餘貨量：</h3>
        </div>        
        <div class="col-6 food-remain">
            <h3>${food.remain}</h3>
        </div>
        <div class="col-6 change"> <button type="button" class="no-button" data-toggle="modal" data-target="#modal-${food.Id}">
            change</button>
            <div class="modal fade" id="modal-${food.Id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-toggle="modal" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="col-2">
                                <h3>Name:</h3>
                                </div>
                                <div class="col-10">
                                    <textarea size="20" name="name" id="name-info" class="name">${food.Name}</textarea>
                                </div>
                                <div class="col-2">
                                <h3>Price:</h3>
                                </div>
                                <div class="col-10">
                                    <textarea size="20" name="price" id="price-info" class="price">${food.Price}</textarea>
                                </div>
                                <div class="col-2">
                                <h3>Remain:</h3>
                                </div>
                                <div class="col-10">
                                    <textarea size="20" name="remain" id="remain-info" class="remain">${food.remain}</textarea>
                                </div>
                                <div class="col-2">
                                <h3>Image:</h3>
                                </div>
                                <div class="col-10">
                                <form class="upload-food-image" data-id=${food.Id}>
                                <input type="file" name="food-pic" id="image-upload">
                                
                                <input type="submit">
                                </form>
                                </div>
                                
                                <div class="confirm-button">
                                    <button type="button" class="real-change-button" data-id=${food.Id}>更改</button>
                                    <button type="button" class="wait-button"
                                        data-dismiss="modal">等等</button>

                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        
        
        <div class="col-6 delete" id="delete-${food.Id}">
            <button data-id="${food.Id}" class="delete-button">delete</button>
        </div>
        </div>
        </div>
        </div>
        `
    }

    addClick = async (event) => {

        this.addedNameInfo = document.querySelector(`#add-name-info`).value
        this.addedPriceInfo = document.querySelector(`#add-price-info`).value
        this.addedRemainInfo = document.querySelector(`#add-remain-info`).value
        this.addedImageInfo = document.querySelector(`#add-image-upload`).files[0].name || null
        this.addedPositionInfo = document.querySelector(`#add-position-info`).value

        this.addFood = {
            Name: this.addedNameInfo,
            Price: parseInt(this.addedPriceInfo),
            remain: parseInt(this.addedRemainInfo),
            image: `uploads/${this.addedImageInfo}`,
            flag: "current"
        }

        await fetch('/foods12/add', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ detail: this.addFood, position: this.addedPositionInfo })
        })

        location.href = './system-management.html'
    }



    changeStatus = (event) => {

        const changedName = document.querySelector(`.block-${event.target.dataset.id} .food-name h3`)
        const changedPrice = document.querySelector(`.block-${event.target.dataset.id} .food-price h3`)
        const changedRemain = document.querySelector(`.block-${event.target.dataset.id} .food-remain h3`)
        const changedImage = document.querySelector(`.block-${event.target.dataset.id} .food-image`)

        this.changedNameInfo = document.querySelector(`#modal-${event.target.dataset.id} #name-info`).value
        this.changedPriceInfo = document.querySelector(`#modal-${event.target.dataset.id} #price-info`).value
        this.changedRemainInfo = document.querySelector(`#modal-${event.target.dataset.id} #remain-info`).value
        this.changeImageInfo = document.querySelector(`#modal-${event.target.dataset.id} #image-upload`).files[0].name

        changedName.innerHTML = this.changedNameInfo
        changedPrice.innerHTML = `($${this.changedPriceInfo})`
        changedRemain.innerHTML = this.changedRemainInfo
        changedImage.innerHTML = `<img src="uploads/${this.changeImageInfo}" class="picture-size w-100">`

        this.foodMenuWithID[event.target.dataset.id].name = this.changedNameInfo
        this.foodMenuWithID[event.target.dataset.id].price = this.changedPriceInfo
        this.foodMenuWithID[event.target.dataset.id].remain = this.changedRemainInfo
        this.foodMenuWithID[event.target.dataset.id].image = `uploads/${this.changeImageInfo}`
    }

    deleteButton = async (event) => {
        console.log('delete?')
        const chosenFood = document.querySelector(`.block-${event.target.dataset.id} .food-image`)
        const deleteWord = document.querySelector(`#delete-${event.target.dataset.id} .delete-button`)

        console.log(this.check)



        let checkDelete = this.check[event.target.dataset.id]





        if (checkDelete == null) {
            chosenFood.innerHTML += `<i class="fas fa-times"></i>`
            deleteWord.innerHTML = "No..."
            this.check[event.target.dataset.id] = event.target.dataset.id - 1
        } else {
            chosenFood.innerHTML = `<img src="${this.foodMenuWithID[event.target.dataset.id].image}" class="picture-size">`
            deleteWord.innerHTML = "delete"
            this.check[event.target.dataset.id] = null
        }

        console.log(this.check)


    }

    finalChange = async (event) => {

        this.foodUpdate = Object.keys(this.foodMenuWithID).map((num) => {
            return {

                Name: this.foodMenuWithID[num].name,
                Price: parseInt(this.foodMenuWithID[num].price),
                remain: parseInt(this.foodMenuWithID[num].remain),
                image: this.foodMenuWithID[num].image,
                flag: "current"
            }
        })

        await fetch('/foods12/delete-clicked', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.check)
        })

        await fetch('/foods12/update', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.foodUpdate)
        })



        location.href = './system-management.html'




    }
}



new SystemManagement()