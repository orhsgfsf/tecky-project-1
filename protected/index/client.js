class Client {
    constructor() {
        this.coupon = document.querySelector(".coupon")
        this.clientCoupon = {}
        this.couponData = []
        this.loadClientData()
        this.loadCurrentCouponData()
    }

    loadCurrentCouponData = async () => {



        // console.log(this.clientCoupon)

        // let buyTimes = Object.keys(this.clientCoupon)
        // console.log(buyTimes)

        // if (buyTimes.length > 9){
        //     for (let i = buyTimes.length - 1; i > buyTimes.length - 10; i--){
        //         this.coupon9.push(buyTimes[i])
        //     }

        //     buyTimes = this.coupon9.sort((a, b) => {
        //         return a - b
        //     })

        //     console.log(buyTimes)


        // }

        const res = await fetch('/client/get-coupon-detailFinal')
        this.currentCouponData = await res.json()
        this.currentCouponData.forEach((o, i) => o.number = i + 1)
        for (let eachCouponData of this.currentCouponData) {
            this.couponData[eachCouponData.number] = { cost: eachCouponData.cost, sale: eachCouponData.sale, time: eachCouponData.time }
        }

        console.log(this.couponData)



        let html = ""

        let buyTimes = Object.keys(this.couponData)
        buyTimes.map((number) => {
            html += `
                    <div data-id="${number}" class="col-4 coupon-container">
                        <div class="coupon-block">
                            <div>消費日期</div>
                            <div class="text-center">${this.couponData[number].time}</div>
                            <div>消費金額</div>
                            <div class="text-center">$${this.couponData[number].cost}</div>
                            <div>獲得優惠</div>
                            <div class="text-center">$${this.couponData[number].sale}</div>
                        </div>
                    </div>
                    `
        })

        this.coupon.innerHTML = html

        // this.couponStorage = {
        //     cost: eachCouponData.cost, 
        //     sale: eachCouponData.sale, 
        //     time: eachCouponData.time,
        //     flag: "current"
        // }

        // await fetch ('/client/storage-coupon', {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify(this.couponStorage)
        // })
    }

    loadClientData = async () => {
        // ///////// This is to load the client's history so it will be put into the loadClientData() later

        const res2 = await fetch('/client/get-history')
        this.clientHistory = await res2.json()

        this.clientHistory.forEach((o, i) => o.purchaseTimes = i + 1)

        for (let client of this.clientHistory) {
            this.clientCoupon[client.purchaseTimes] = { purchaseTotal: client.purchaseTotal, sale: client.sale, purchaseTime: client.purchaseTime }
        }
    }




}

let client = new Client()