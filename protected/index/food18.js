const socket = io.connect()


class Done {
    constructor(){
        this.sale = document.querySelector('.statement.total')        
        this.food = {}
        this.realCost = JSON.parse(localStorage.getItem('value') || null )
        this.foodRemain = []
        this.foodUpdate = []
        this.foodPurchase = null
        this.checkOK()
        this.logout()
        this.clientCoupon = {}
        this.coupon9 = []
        this.getSelected()
        this.address = JSON.parse(localStorage.getItem('address') || "")
        
       
        
       
        
        
    }

    checkOK = async () => {
        
        const back = document.querySelector('.continue-button')
        back.addEventListener('click', this.backClick)

        const res5 = await fetch('/foods13/get')
        this.foodPurchase = await res5.json()
        if (this.realCost <= 50){
            this.realCost = 50
        }
        console.log(this.realCost)
        this.saleMoney = this.realCost * 0.1
        console.log(this.saleMoney)
        this.sale.innerHTML = `$${this.saleMoney}`

        this.checkClick = JSON.parse(localStorage.getItem('click?') || 0 )
        if (this.checkClick == 0){            
            return
        }

        this.click = 0

        localStorage.setItem('click?', JSON.stringify(this.click))
       

       
        
        const res = await fetch('/foods13/get')
        this.foodPurchase = await res.json()
        if (this.foodPurchase == null){
            location.href = "./page12.html"
            return
        }

      

        await this.loadFoodRemain()       
        await this.loadValue()
        
    }

    getSelected = async () => {

        const selected = await fetch ('/client/get-selected-coupon')
        this.selected = await selected.json()
        console.log(this.selected)

    }


    loadFoodRemain = async () => {

        // const selected = await fetch ('/client/get-selected')
        // this.selected = await selected.json()
        // console.log(this.selected)



        const res2 = await fetch('/foods12/get')
        this.foods = await res2.json()
        console.log(this.foods)
        for (let food of this.foods) {
            
            this.food[food.Id] = { name: food.Name, id: food.Id, price: food.Price, remain: food.remain, image: food.image }
        }

        await this.check()
        

        console.log(this.food)
        console.log(this.foodPurchase)
    }

    check = async () => {
        Object.keys(this.foodPurchase.detail).map(async (id) => {
            if (this.foodPurchase.detail[id].qty > this.food[id].remain) {
                const res3 = await fetch('/foods13/clear')
                this.clearAll = await res3.json()
                console.log(this.clearAll)
                location.href = './page12.html'
            }
        })
    }



    loadValue = async () => {
        const res4 = await fetch('/foods13/get')
        this.foodPurchase = await res4.json()
        if (this.foodPurchase == null){
            location.href = './page12.html'
            return
        }
        await fetch('/foods12/markDownRecord')
        
        this.foodRemain = Object.keys(this.foodPurchase.detail).map((id) => {
            return (this.food[id].remain - this.foodPurchase.detail[id].qty)
        })

        this.foodUpdate = Object.keys(this.foodPurchase.detail).map((id) => {
            return { Name: this.food[id].name, Price: this.food[id].price, remain: this.foodRemain[id - 1], image: this.food[id].image, flag: "current"}
        })

        //  Id: this.food[id].id

        console.log(this.foodUpdate)
        await fetch('/foods12/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.foodUpdate)
        })
        

        // const res5 = await fetch('/foods13/get')
        // this.foodPurchase = await res5.json()
        // console.log(this.realCost)
        // this.saleMoney = this.realCost * 0.1
        // console.log(this.saleMoney)
        // this.sale.innerHTML = `$${this.saleMoney}`

        const logout = document.querySelector('.logout-button')
        logout.addEventListener('click', this.logoutClick)



        await this.clientDetailUpload()
    }

    clientDetailUpload = async() => {

        if (this.realCost <= 50){
            this.realCost = 50
        }

        // this.clientPurchase = {
        //     purchase: this.realCost, 
        //     sale: this.saleMoney
        // }

        // await fetch('/client/upload', {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify({ data: this.clientPurchase })

            
        // })

        // const resH = await fetch('/client/get-history')
        // this.clientHistory = await resH.json()
        
        // this.clientHistory.forEach((o,i) => o.purchaseTimes = i+1)
                
        // for (let client of this.clientHistory){
        //         this.clientCoupon[client.purchaseTimes] = {purchaseTotal: client.purchaseTotal, sale: client.sale, purchaseTime: client.purchaseTime}
        //     }
               
        // console.log(this.clientCoupon)

        // let buyTimes = Object.keys(this.clientCoupon)
       

        // if (buyTimes.length > 9){
        //     for (let i = buyTimes.length - 1; i > buyTimes.length - 10; i--){
        //         this.coupon9.push(buyTimes[i])
        //     }
    
        //     buyTimes = this.coupon9.sort((a, b) => {
        //         return a - b
        //     })                     
        // }




        const resClock = await fetch('/client/clock')
        this.clock = await resClock.json()
    
        this.clientPurchaseDetail = { 
            purchaseDetail: this.foodPurchase.detail,
            purchaseTotal: this.foodPurchase.total,
            purchaseTotalAfterCoupon: this.realCost,
            sale: this.saleMoney,
            purchaseTime: this.clock,
            address: this.address
        }


        await fetch('/client/upload-user-data', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.clientPurchaseDetail)
            
        })


       

        

        socket.emit('client-detail', (this.clientPurchaseDetail))
 

        
        
        
        
        const res = await fetch('/foods13/clear')
        this.clearAll = await res.json()  
        
        this.couponDetail = {
            cost: this.realCost,
            sale: this.saleMoney,
            time: this.clock,
            flag: "current"
        }
        await fetch('/client/update-coupon-status', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.couponDetail)

            
        })
        
    }

    logout = () =>{

        const logout = document.querySelector('.logout-button')
        logout.addEventListener('click', this.logoutClick)
    }

    backClick = async (event) => {        
        location.href = './page12.html'
    }

    logoutClick = async (event) => {
        await fetch('/logout')
        location.href = '/index.html'
    }
}

new Done()