


// const socket = io.connect()




// const orderedFood = document.querySelector('.ordered-food')
// const orderedTotal = document.querySelector('.total')
// let totalCost = 0

// let html = ''

// // io.on('connection', function (socket) {
//     socket.on('client-detail', (msg) => {


//         const time = renderTime(msg.purchaseTime)
//         const total = renderTotal(msg.purchaseTotalAfterCoupon)
//         const number = Object.keys(msg.purchaseDetail)
//         let foods = ''
//         for (let num of number){
//             if (msg.purchaseDetail[num].qty != 0){
//                 foods += renderFood(msg.purchaseDetail[num])
//             }

//         }
//         html += `<div class="row purchase-description"> ${time} ${foods} ${total} </div>`
//         totalCost += msg.purchaseTotalAfterCoupon
//         orderedFood.innerHTML = html
//         orderedTotal.innerHTML = `<h3>總額：$${totalCost} </h3>`

//     })
//             renderFood = (food) => {
//                 return ` <div class="col-6 food statement-center2">
//                 <div class=food-name"></div>
//                 <div>               
//                     <h3>${food.name}</h3>
//                 </div>
//             </div>
//             <div class="col-6 food statement-qty-center">
//                 <div>
//                     <h3>x ${food.qty}</h3>
//                 </div>
//             </div>`
//             }

//             renderTotal = (total) => {
//                 return `<div class="col-12">
//                 <h3>$${total}</h3>
//                 </div>`
//             }

//             renderTime = (time) => {
//                 return `<div class="col-12">
//                 <h3>${time}</h3>
//                 </div>`
//             }
//     // socket.request.session.socketId = socket.id;
//     // socket.request.session.save();
// ;


const socket = io.connect()



class CheckOrder {

    constructor() {

        this.orderedFood = document.querySelector('.ordered-food')
        this.orderedTotal = document.querySelector('.total')

        this.foodOrder = {}
        this.loadOrder()
        this.totalCost = 0

        // In boss general page
        this.checkOrder()
        this.system()
        this.logout = document.querySelector('.logout-button button')
        this.logout.addEventListener('click', this.logOut)


    }

    loadOrder = async () => {

        const res = await fetch('/client/get-food-order')
        this.getFoodOrder = await res.json()
        this.getFoodOrder.forEach((o, i) => o.number = i + 1)
        console.log(this.getFoodOrder)

        for (let order of this.getFoodOrder) {
            this.foodOrder[order.number] = { detail: order.purchaseDetail, cost: order.purchaseTotalAfterCoupon, time: order.purchaseTime, totalCost: order.totalCost, address: order.address }

        }
        console.log(this.foodOrder)

        const numB = Object.keys(this.foodOrder)
        console.log(numB.length)




        const res3 = await fetch('/client/get-money')
        this.money = await res3.json()



        let htmlx = ''
        for (let num of numB) {


            if (numB.length > 0) {
                this.detailNumber = Object.keys(this.foodOrder[num].detail)

            } else {
                numB.length = 0
            }

            if (numB.length > 0) {
                this.currentCost = this.foodOrder[numB.length].totalCost
            } else {
                numB.length = 0
            }

            const orderedTotal = this.renderTotal(this.foodOrder[num].cost)
            const orderedTime = this.renderTime(this.foodOrder[num].time)
            const buttonWithId = this.buttonID(num)
            const addressOfClient = this.renderAddress(this.foodOrder[num].address)

            let orderedDetail = ''
            for (let detailNum of this.detailNumber) {


                if (this.foodOrder[num].detail[detailNum].qty != 0) {
                    orderedDetail += this.renderFood(this.foodOrder[num].detail[detailNum])
                }
            }htmlx += `<div class="row purchase-description" data-id="${num}"> ${orderedTime} ${orderedDetail} ${orderedTotal} ${addressOfClient} ${buttonWithId}</div>`
        }



        this.orderedFood.innerHTML = htmlx
        this.orderedTotal.innerHTML = `<h3>總額：$${this.money} </h3>`



        console.log(this.foodOrder)

        let html = ''
        // io.on('connection', function (socket) {
        socket.on('boss-client-detail', async (msg) => {

            const res3 = await fetch('/client/get-money')
            this.money = await res3.json()

            const time = this.renderTime(msg.purchaseTime)
            const total = this.renderTotal(msg.purchaseTotalAfterCoupon)
            const number = Object.keys(msg.purchaseDetail)
            const buttonID = this.buttonID(msg.number)
            const clientAddress = this.renderAddress(msg.address)
            let foods = ''
            for (let num of number) {
                if (msg.purchaseDetail[num].qty != 0) {
                    foods += this.renderFood(msg.purchaseDetail[num])
                }

            }


            html = `<div class="row purchase-description data-id="${msg.number}"> ${time} ${foods} ${total} ${clientAddress} ${buttonID} </div>`


            this.totalCost += msg.purchaseTotalAfterCoupon
            this.orderedFood.innerHTML += html
            this.orderedTotal.innerHTML =

                `<h3>總額：$${this.money} </h3>`
            console.log(this.totalCost)

            this.addClearEvent()






        })

        this.addClearEvent()
        //}




    }

    system = () => {
        document.querySelector('button.system-management').addEventListener('click', this.enterToManagementPage)
    }

    checkOrder = () => {
        document.querySelector('button.check-order').addEventListener('click', this.enterToOrderPage)

    }

    enterToManagementPage = () => {
        location.href = "./system-management.html"
    }

    enterToOrderPage = () => {
        location.href = './check-order.html'
    }

    addClearEvent = () => {
        const clearButton = document.querySelectorAll('.buttondiv button')
        for (let button of clearButton) {

            button.addEventListener('click', this.onClearClick)
        }
    }
    renderFood = (food) => {
        return ` <div class="col-6 food statement-center2">
                    <div class=food-name"></div>
                    <div>               
                        <h3>${food.name}</h3>
                    </div>
                </div>
                <div class="col-6 food statement-qty-center">
                    <div>
                        <h3>x ${food.qty}</h3>
                    </div>
                </div>`
    }

    renderTotal = (total) => {
        return `<div class="col-12">
                    <h3>$${total}</h3>
                    
                    </div>`
    }

    renderTime = (time) => {
        return `<div class="col-12">
                    <h3>${time}</h3>
                    </div>`
    }

    renderAddress = (address) => {
        return `
        <div class="col-12 address">
            <h3>地址：${address}</h3>
        </div>
        `
    }

    buttonID = (id) => {
        return `<div class="buttondiv">
        <div><button data-id=${id}>X</button></div>
       
        </div> `

        //<i class="fas fa-times"></i><
    }


    onClearClick = async (event) => {

        document.querySelector(`div[data-id="${event.target.dataset.id}"]`)
        const res4 = await fetch('/client/get-food-order')
        this.foodOrder = await res4.json()

        // for (let i = 0; i < this.foodOrder.length - 1; i++){
        // if ( i == event.target.dataset.id - 1){
        // this.foodOrder[event.target.dataset.id - 1]
        console.log(123123123123)
        await fetch('/client/delete-ordered', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id: event.target.dataset.id - 1 })
        })
        location.href = "/protected/check-order.html"
        // }
        // }



        //         // this.foodOrder[event.currentTarget.dataset.id] = {
        //         //     ...this.foodOrder[event.currentTarget.dataset.id] = {


        // }
        //event.target.parentElement.dataset.id
    }

    logOut = async () => {

        await fetch('/logout')
        location.href = '/index.html'
    }









    // socket.request.session.socketId = socket.id;
    // socket.request.session.save();


}

new CheckOrder()
