// List Food Item
class ListFood {
    constructor() {
        this.menu = document.querySelector('#menu')
        this.member = document.querySelector('.member-name')
        this.user = {}
        this.selectedFood = {}
        this.foodRemain = {}
        this.foodPurchase = null
        this.loadFood()
    }

    loadFood = async () => {
        const res1 = await fetch('/client/get-user-data')
        this.getClients = await res1.json()
       
        for (let client of this.getClients){
            this.user[client.id] = {username: client.username}
        }
        console.log(this.user)

        const resID = await fetch ('/client/getID')
        this.getID = await resID.json()


        this.member.innerHTML += `<h4> 會員 <u>${this.user[this.getID].username}</u>: 您好！`

        const res = await fetch('/foods12/get')
        this.foods = await res.json()
        console.log(this.foods)

        let html = ''
        for (let food of this.foods) {
            this.selectedFood[food.Id] = { name: food.Name, qty: 0, price: food.Price }
            this.foodRemain[food.Id] = { remain: food.remain }
            html += this.renderFoodRow(food)
        }
        console.log(this.foodRemain)


        this.menu.innerHTML = html

        

        const addButton = menu.querySelectorAll('button.add-button')
        for (let button of addButton) {
            button.addEventListener('click', this.onAddClick)
        }

        const subtractButton = menu.querySelectorAll('button.subtract-button')
        for (let button of subtractButton) {
            button.addEventListener('click', this.onSubtractClick)
        }

        const clearButton = menu.querySelectorAll('button.clear-button')
        for (let button of clearButton) {
            button.addEventListener('click', this.onClearClick)
        }

        const checkRecordButton = document.querySelector('button.check-my-record')
        checkRecordButton.addEventListener('click', this.checkMyRecord)       

        const clearAllButton = document.querySelector('button.clear-all-button')
        clearAllButton.addEventListener('click', this.onAllClearClick)

        const purchaseButton = document.querySelector('button.purchase-button')
        purchaseButton.addEventListener('click', this.onPurchaseClick)

        const logout = document.querySelector('.logout-button')
        logout.addEventListener('click', this.logOut)

        await this.reloadPurchaseList()
    }

    reloadPurchaseList = async () => {
        // this.foodPurchase = JSON.parse(localStorage.getItem('foodPurchase') || {})
        const res = await fetch('/foods13/get')
        try{
            this.foodPurchase = await res.json()
        }catch{
            this.foodPurchase = null
        }
        console.log(this.foodPurchase)


        if (this.foodPurchase && Object.keys(this.foodPurchase.detail).length > 0) {
            const foodDetail = this.foodPurchase.detail;
            Object.keys(foodDetail).map((key) => {
                document.querySelector(`div[data-id="${key}"] .qua .number h3`).innerHTML = foodDetail[key].qty
            })
            this.selectedFood = foodDetail
            this.calculateTotal()
        }

    }

    renderFoodRow(food) {
        return `
        <div data-id=${food.Id} class="row food-detail">            
            <div class="food-intro">            
                <img src="${food.image}" class="beef-picture-size">                
            </div>
            <div class="beef-rice">
                <div class="beef-description">
                    <h3>${food.Name} ($${food.Price})</h3>
                </div>
                <div class="add-subtract-button">
                    <div class="row">
                        <div class="col-6">
                            <h3><button data-id="${food.Id}" class="add-button"><b>+</b></button>
                        </div>
                        <div class="col-6">
                            <h3><button data-id="${food.Id}" class="subtract-button"><b>-</b></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="qua">
                <div class="quantity">
                    <h3>數量:</h3>
                </div>
                <div class="number">
                    <h3>0</h3>
                </div>
                <div class="clear-button">
                    <button data-id="${food.Id}" class="clear-button"><b>清除</b></button>
                </div>
            </div>
       </div>
            
            `


    }

    checkMyRecord = () => {
       
        location.href = './check-my-record.html'
    }

    calculateTotal = () => {
        const total = document.querySelector('.food-intro-statement2').querySelector('.total-number h3')
        const result = Object.keys(this.selectedFood).reduce((total, key) => {
            return total += this.selectedFood[key].qty * this.selectedFood[key].price
        }, 0)
        total.innerHTML = result


        
        return result
    }



    onAddClick = (event) => {
        // const food = this.foods.find(food => food.Id == event.currentTarget.dataset.id)
        // console.log(food)

        const number = this.menu.querySelector(`div[data-id="${event.currentTarget.dataset.id}"]`).querySelector('.number h3')
        const food = this.selectedFood[event.currentTarget.dataset.id]
        const remain = this.foodRemain[event.currentTarget.dataset.id]
        

        if (food.qty < remain.remain) {

            this.selectedFood =
                {
                    ...this.selectedFood,
                    [event.currentTarget.dataset.id]: {
                        ...food,
                        qty: food.qty + 1
                    }
                }

            number.innerHTML++
            this.calculateTotal()
        } else {
            event.preventDefault()
        }

        // const total = document.querySelector('.food-intro-statement2').querySelector('.total-number h3')        
        // total.innerHTML = (parseInt(number.innerHTML)* parseInt(food.Price))
        // this.total += parseInt(food.Price)
        // total.innerHTML = this.total
    }

    onSubtractClick = (event) => {
        // const food = this.foods.find(food => food.Id == event.currentTarget.dataset.id)
        // console.log(food)

        const number = this.menu.querySelector(`div[data-id="${event.currentTarget.dataset.id}"]`).querySelector('.number h3')
        const food = this.selectedFood[event.currentTarget.dataset.id];
        if (parseInt(number.innerHTML) > 0) {
            this.selectedFood =
                {
                    ...this.selectedFood,
                    [event.currentTarget.dataset.id]: {
                        ...food,
                        qty: food.qty - 1
                    }
                }

            number.innerHTML--
        }

        this.calculateTotal()

        // const total = document.querySelector('.food-intro-statement2').querySelector('.total-number h3')

        // total.innerHTML = parseInt(number.innerHTML) * parseInt(food.Price)

        // this.total -= (parseInt(food.Price))
        // total.innerHTML = this.total
    }

    onClearClick = (event) => {
        // const food = this.foods.find(food => food.Id == event.currentTarget.dataset.id)
        // console.log(food)
        const number = this.menu.querySelector(`div[data-id="${event.currentTarget.dataset.id}"]`).querySelector('.number h3')
        const total = document.querySelector('.food-intro-statement2').querySelector('.total-number h3')
        const food = this.selectedFood[event.currentTarget.dataset.id];

        if (number.innerHTML > 0) {
            this.selectedFood =
                {
                    ...this.selectedFood,
                    [event.currentTarget.dataset.id]: {
                        ...food,
                        qty: 0,
                    }
                }

            number.innerHTML = 0
        }

        this.calculateTotal()
        // total.innerHTML = 0
    }

    onAllClearClick = (event) => {
        //const number = this.menu.querySelector(`div[data-id="${event.currentTarget.dataset.id}"]`) .querySelector('.number h3')
        // const total = document.querySelector('.food-intro-statement2').querySelector('.total-number h3')
        // const food = this.selectedFood[event.currentTarget.dataset.id];

        const numberh3 = this.menu.querySelectorAll('.number h3')
        for (let x of numberh3) {
            x.innerHTML = 0
        }

        let ID = (Object.keys(this.selectedFood))

        /* 
        this.selectedFood = {
            0: { qty: 2, price: 100}
            1: { qty: 2, price: 200}
        }

        this.selectedFoodNew = {
            0: { qty: 2, price: 100}
            1: { qty: 2, price: 200}
        }

        this.selectedFoodNew = {
            0New: { qty: 3, price: 100}
            1: { qty: 2, price: 200}
        }
        */
        for (let id of ID) {



            (this.selectedFood[id].qty = 0)
        }
        this.calculateTotal()
    }
    onPurchaseClick = async (event) => {
        
        this.foodDetail = {
            detail: this.selectedFood,
            total: this.calculateTotal()
        }
        await fetch('/foods12/post', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.foodDetail)
        })
        // localStorage.setItem('foodPurchase', JSON.stringify(this.foodDetail))

        Object.keys(this.foodRemain).map(async (id) => {
            if (this.selectedFood[id].qty > this.foodRemain[id].remain) {
                const res = await fetch('/foods13/clear')
                this.clearAll = await res.json()
                console.log(this.clearAll)
                location.href = './page12.html'
            }
        })

        if (this.foodDetail.total != 0) {
            location.href = './page15.html'
        } else {
            event.preventDefault()
        }
    }

    logOut = async () => {
        await fetch('/foods13/clear')
        await fetch('/logout')
        location.href = '/index.html'
    }

    


}

window.listFood12 = new ListFood()
