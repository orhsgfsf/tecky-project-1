class ListPurchase {
    constructor() {
        this.purchase = document.querySelector('#purchase')
        this.coupon = document.querySelector(".coupon")
        this.checkCoupon = {}
        this.couponData = []
        this.loadPurchase()
        this.loadClientData()
        this.click = 0       
        this.foodPurchase = null
        this.food = {}
        this.foodRemain = []
        this.foodUpdate = []
        this.clientCoupon = {}
        this.coupon9 = []
        this.count = 0
        this.checkAddress = document.querySelector('.check-address')

  
        
       
       
    }

    loadClientData = async () => {

        const res = await fetch('/client/get-coupon-detailFinal')
        this.currentCouponData = await res.json()
        this.currentCouponData.forEach((o, i) => o.number = i + 1)
        for (let eachCouponData of this.currentCouponData) {
            this.couponData[eachCouponData.number] = { cost: eachCouponData.cost, sale: eachCouponData.sale, time: eachCouponData.time }
        }

        // const res = await fetch('/client/get-history')
        // this.clientHistory = await res.json()
        
        // this.clientHistory.forEach((o,i) => o.purchaseTimes = i+1)
                
        // for (let client of this.clientHistory){
        //         this.clientCoupon[client.purchaseTimes] = {purchaseTotal: client.purchaseTotal, sale: client.sale, purchaseTime: client.purchaseTime}
        //     }
               
        // console.log(this.clientCoupon)

        // let buyTimes = Object.keys(this.clientCoupon)
        // console.log(buyTimes)

        // if (buyTimes.length > 9){
        //     for (let i = buyTimes.length - 1; i > buyTimes.length - 10; i--){
        //         this.coupon9.push(buyTimes[i])
        //     }
    
        //     buyTimes = this.coupon9.sort((a, b) => {
        //         return a - b
        //     })

        //     console.log(buyTimes)            
        // }
        


        let html = ""

        let buyTimes = Object.keys(this.couponData)
        buyTimes.map((number) => {
            html += `
                    <div data-id="${number}" class="col-4 coupon-container">
                        <div class="coupon-block">
                            <div>消費日期</div>
                            <div class="text-center">${this.couponData[number].time}</div>
                            <div>消費金額</div>
                            <div class="text-center">$${this.couponData[number].cost}</div>
                            <div>獲得優惠</div>
                            <div class="text-center">$${this.couponData[number].sale}</div>
                        </div>
                    </div>
                    `
        })

        this.coupon.innerHTML = html

        if (html != ""){
            let statement = document.querySelector(".statement")
            statement.innerHTML = '<h3>使用優惠券：</h3>'
        }
        
        this.clickCoupon = document.querySelectorAll('.coupon-container')
        for (let coupon of this.clickCoupon){
            coupon.addEventListener('click', this.couponClick)
        }

        

    }

    loadPurchase = async () => {
        const res = await fetch('/foods13/get')
        this.foodPurchase = await res.json()
        if (this.foodPurchase == null) {
            location.href = './page12.html'
            return
        }

        console.log(this.foodPurchase)
        // this.foodPurchase = JSON.parse(localStorage.getItem('foodPurchase') || {})


        let html = ''
        const foodPurchaseID = Object.keys(this.foodPurchase.detail)
        console.log(foodPurchaseID)

        for (let id of foodPurchaseID) {

            if (this.foodPurchase.detail[id].qty != 0) {
                html += this.RenderPurchase(this.foodPurchase.detail[id])
            }
        }
        console.log(this.foodPurchase.detail)
        this.purchase.innerHTML = html

        this.calTotal(this.foodPurchase.total)

        const eatMoreButton = document.querySelector('button.reg-button')
        eatMoreButton.addEventListener('click', this.eatMoreClick)

       

        const confirmButton = document.querySelector('button.no-button')
        confirmButton.addEventListener('click', this.confirmClick)

        await this.loadFoodRemain()
    }

    loadFoodRemain = async () => {
        const res = await fetch('/foods12/get')
        this.foods = await res.json()
        for (let food of this.foods) {
            this.food[food.Id] = { name: food.Name, id: food.Id, price: food.Price, remain: food.remain, image: food.image }
        }

        await this.check()

        console.log(this.food)
        console.log(this.foodPurchase)

       
    }

    check = async () => {

        if (this.foodPurchase == null) {
            location.href = './page12.html'
            return
        }
        Object.keys(this.foodPurchase.detail).map(async (id) => {
            if (this.foodPurchase.detail[id].qty > this.food[id].remain) {
                const res2 = await fetch('/foods13/clear')
                this.clearAll = await res2.json()
                console.log(this.clearAll)
                location.href = './page12.html'
            }
        })
    }




    RenderPurchase(purchase) {
        return `
        <div class="row purchase-description">
            <div class="col-6">
                <div>               
                <h3>${purchase.name}</h3>
                </div>
            </div>
            <div class="col-3">
                <div>
                <h3>${purchase.qty}</h3>
                </div>
            </div>
            <div class="col-3">     
                <div>         
                <h3>${purchase.price * purchase.qty}</h3>
                </div>
            </div>
        </div>
        `
    }

    calTotal = (total) => {
        const totalNum = document.querySelector(".row.ttl").querySelector(".total-number h3")
        totalNum.innerHTML = total
        return total


    }

    couponClick = (event) => {
        
        const saleCost = this.coupon.querySelector(`div[data-id="${event.currentTarget.dataset.id}"]`)
        const cost = document.querySelector(".total-number h3")
        
        // const costAfterSale = this.calTotal(this.foodPurchase.total) - saleCost.innerHTML
        // const findCoupon = this.checkCoupon.find(coupon => coupon.id === event.currentTarget.dataset.id)
        let findCoupon = this.checkCoupon[event.currentTarget.dataset.id] 

        console.log(findCoupon)
      
            if(findCoupon == null){
                if (this.count < 3){
                    this.count++
                    this.foodPurchase.total = this.foodPurchase.total - this.couponData[event.currentTarget.dataset.id].sale
                    saleCost.innerHTML += `<div class="tick"><i class="far fa-check-circle"></i></div>`

                    // findCoupon = true
                    this.checkCoupon[event.currentTarget.dataset.id] = event.currentTarget.dataset.id - 1
                }else{
                    event.preventDefault()
                }
            }else{
                this.count--
                this.foodPurchase.total = this.foodPurchase.total + this.couponData[event.currentTarget.dataset.id].sale
               
                // findCoupon = false
                saleCost.innerHTML = `
                <div data-id="${event.currentTarget.dataset.id}">
                    <div class="coupon-block">
                        <div>消費日期</div>
                        <div class="text-center">${this.couponData[event.currentTarget.dataset.id].time}</div>
                        <div>消費金額</div>
                        <div class="text-center">$${this.couponData[event.currentTarget.dataset.id].cost}</div>
                        <div>獲得優惠</div>
                        <div class="text-center">$${this.couponData[event.currentTarget.dataset.id].sale}</div>
                    </div>
                </div>
                `
                this.checkCoupon[event.currentTarget.dataset.id] = null
            }
        
            if (this.foodPurchase.total <= 50){
                cost.innerHTML = 50
                
            }else{
                cost.innerHTML = this.foodPurchase.total
            }
            console.log(this.foodPurchase.total)
            console.log(this.checkCoupon)
    }

    eatMoreClick = (event) => {
        
        location.href = './page12.html'
    }

    confirmClick = async (event) => {
        this.info = document.querySelector('#address-info').value
        console.log(typeof("    "))


        if (this.info.trim() == ""){
            this.checkAddress.innerHTML = `
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" data-toggle="modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h1>你唔寫地址我點送去比你呀？</h1>
                    </div>
                    <div class="confirm-button">
                       
                        <button type="button" class="real-purchase-button sorry"
                            data-dismiss="modal">Sor lo~</button>

                    </div>
                </div>
            </div>
        </div>`
        }else{
            this.checkAddress.innerHTML = `<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" data-toggle="modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h1>確定結帳嗎？</h1>
                    </div>
                    <div class="confirm-button">
                        <button type="button" class="real-buy-purchase-button">結啦咁＿多野講</button>
                        <button type="button" class="real-purchase-button"
                            data-dismiss="modal">仲有野想食，暫時不了</button>

                    </div>
                </div>
            </div>
        </div>`
        const doneButton = document.querySelector('button.real-buy-purchase-button')
        doneButton.addEventListener('click', this.doneClick)
        }

    }

    doneClick = async (event) => {

        

        this.click++



        localStorage.setItem('address', JSON.stringify(this.info))      
        localStorage.setItem('click?', JSON.stringify(this.click))
        localStorage.setItem('value', JSON.stringify(this.foodPurchase.total))
        
        console.log(this.checkCoupon)

        await fetch('/client/upload-selected-coupon', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.checkCoupon)
            

        })
       

        // await fetch('/foods12/markDownRecord')
        // console.log(this.foodPurchase.detail)

        // this.foodRemain = Object.keys(this.foodPurchase.detail).map((id) => {
        //     return (this.food[id].remain - this.foodPurchase.detail[id].qty)
        // })

        // this.foodUpdate = Object.keys(this.foodPurchase.detail).map((id) => {
        //     return { Name: this.food[id].name, Id: this.food[id].id, Price: this.food[id].price, remain: this.foodRemain[id - 1], image: this.food[id].image }
        // })
        // console.log(this.foodUpdate)
        // await fetch('/foods12/update', {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     body: JSON.stringify(this.foodUpdate)
        // })

        location.href = './page18.html'

    }


}

// // loadPurchase = async() => { 
//         const foods = Object.keys(window.purchaseList).map((key) => {
//             const food = window.purchaseList[key]
//             return { Id: key, Name: food.name, Price: food.price }
//         })
//     }
// }

// should assign listPurchase to variable
let listPurchase = new ListPurchase()


