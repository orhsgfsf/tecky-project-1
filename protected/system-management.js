class SystemManagement {
    constructor() {

        this.loadFoodMenu()
        this.foodMenuWithID = {}
        this.menu = document.querySelector('.menu')
        this.foodUpdate = []



    }

    loadFoodMenu = async () => {


        const res = await fetch('/foods12/get')
        this.foodMenu = await res.json()

        let html = ''
        for (let foodMenu of this.foodMenu) {
            this.foodMenuWithID[foodMenu.Id] = { name: foodMenu.Name, price: foodMenu.Price, remain: foodMenu.remain, image: foodMenu.image }
            html += this.renderFoodMenu(foodMenu)
            
        }

        // const number = Object.keys(this.foodMenuWithID)

        // let html = ''
        // for (let num of number){
        //     html += this.renderFoodMenu(this.foodMenuWithID[num])
        // }

        this.menu.innerHTML = html

        // for (let foodMenu of this.foodMenu) {
        //     const changeButton = document.querySelector(`div[data-id="${foodMenu.Id}"] .change`)
        //     changeButton.addEventListener('click', this.change.bind(this, foodMenu))
        // }

        // for (let foodMenu of this.foodMenu) {
        //     const realChangeButton = document.querySelector(`div[data-id="${foodMenu.Id}"]`)
        // }


        // const changePrice = document.querySelectorAll('.price')
        // changePrice.addEventListener('click', this.changePriceClick)

        // const changeRemain = document.querySelectorAll('.remain')
        // changeRemain.addEventListener('click', this.changeRemainClick)

        // const changeButton = this.menu.querySelectorAll('.change')
        // let i = 0;
        // for (let button of changeButton){
           
        //     i ++;
        // }

        const change = document.querySelectorAll('.fade .real-change-button')
        for (let button of change){
            button.addEventListener('click', this.changeStatus)
        }

        const finalChange = document.querySelector(`.final-change`)
        finalChange.addEventListener('click', this.finalChange)



    }


    renderFoodMenu = (food) => {
        return `
        <div class="block-${food.Id}">
        <div class="row">
        <div class="col-12 food-image">
            <img src="${food.image}" class="picture-size"> 
        </div>
        
        
        <div class="col-6 food-name">
            <h3>${food.Name}</h3>
        </div>
        <div class="col-6 food-price">
            <h3>($${food.Price})</h3>
        </div> 
        
       
       
        <div class="col-6">
            <h3> 剩餘貨量： </h3>
        </div>        
        <div class="col-6 food-remain">
            <h3>${food.remain}</h3>
        </div>
        <div class="col-6 change"> <button type="button" class="no-button" data-toggle="modal" data-target="#modal-${food.Id}">
            change</button>
            <div class="modal fade" id="modal-${food.Id}" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" data-toggle="modal" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="col-2">
                                <h3>Name:</h3>
                                </div>
                                <div class="col-10">
                                    <textarea size="20" name="name" id="name-info" class="name">${food.Name}</textarea>
                                </div>
                                <div class="col-2">
                                <h3>Price:</h3>
                                </div>
                                <div class="col-10">
                                    <textarea size="20" name="price" id="price-info" class="price">${food.Price}</textarea>
                                </div>
                                <div class="col-2">
                                <h3>Remain:</h3>
                                </div>
                                <div class="col-10">
                                    <textarea size="20" name="remain" id="remain-info" class="remain">${food.remain}</textarea>
                                </div>
                                <div class="col-2">
                                <h3>Image:</h3>
                                </div>
                                <div class="col-10">
                                    <form action="/food-picture" method="POST">
                                    <input type="text" name="file">
                                    <input type="submit">
                                    </form>
                                </div>
                                
                                <div class="confirm-button">
                                    <button type="button" class="real-change-button" data-id=${food.Id}>更改</button>
                                    <button type="button" class="wait-button"
                                        data-dismiss="modal">等等</button>

                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        
        
        <div class="col-6 delete">
            <button data-id="${food.Id}">delete</button>
        </div>
        </div>
        </div>
        </div>
        `
    }



    changeStatus = (event) => {
        console.log(event.target.dataset.id)
        const changedName = document.querySelector(`.block-${event.target.dataset.id} .food-name h3`)
        const changedPrice = document.querySelector(`.block-${event.target.dataset.id} .food-price h3`)
        const changedRemain = document.querySelector(`.block-${event.target.dataset.id} .food-remain h3`)
        console.log(changedName.innerHTML)
        console.log(changedPrice.innerHTML)
        console.log(changedRemain.innerHTML)

        this.changedNameInfo = document.querySelector(`#modal-${event.target.dataset.id} #name-info`).value
        this.changedPriceInfo = document.querySelector(`#modal-${event.target.dataset.id} #price-info`).value
        this.changedRemainInfo = document.querySelector(`#modal-${event.target.dataset.id} #remain-info`).value

        console.log(this.changedNameInfo)
        console.log(this.changedPriceInfo)
        console.log(this.changedRemainInfo)
        
        changedName.innerHTML = this.changedNameInfo
        changedPrice.innerHTML = `($${this.changedPriceInfo})`
        changedRemain.innerHTML = this.changedRemainInfo


        this.foodMenuWithID[event.target.dataset.id].name = this.changedNameInfo
        this.foodMenuWithID[event.target.dataset.id].price = this.changedPriceInfo
        this.foodMenuWithID[event.target.dataset.id].remain = this.changedRemainInfo

        

    }

    finalChange = async (event) => {

        this.foodUpdate = Object.keys(this.foodMenuWithID).map((num) => {
            return {
                Id: num, 
                Name: this.foodMenuWithID[num].name, 
                Price: parseInt(this.foodMenuWithID[num].price),
                remain: parseInt(this.foodMenuWithID[num].remain),
                image: this.foodMenuWithID[num].image
            }
        })

        await fetch ('/foods12/update',{
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(this.foodUpdate)
        })      

        location.href = './system-management.html'
        

        

    }

        



        


        // await fetch ()
    
}

    // change = (food) => {
    //     const name = this.menu.querySelector(`.modal div[data-id="${event.target.dataset.id}"]`).querySelector('.name')
    //     const price = this.menu.querySelector(`.modal div[data-id="${event.target.dataset.id}"]`).querySelector('.price')
    //     const remain = this.menu.querySelector(`.modal div[data-id="${event.target.dataset.id}"]`).querySelector('.remain')

    //     name.innerHTML = `<textarea size="20" name="name" id="name-info">${food.Name}</textarea>`
    //     price.innerHTML = ` <textarea size="20" name="price" id="price-info">${food.Price}</textarea>`
    //     remain.innerHTML = ` <textarea size="20" name="remain" id="remain-info">${food.remain}</textarea>`
    // }



new SystemManagement()