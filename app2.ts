// No Use

import * as express from 'express'
import { FoodRouter12 } from './protected/Routers/Router12'
import { FoodRouter13 } from './protected/Routers/Router13'
import bodyParser = require('body-parser');
import * as expressSession from 'express-session'
import { FoodService13 } from './protected/Services/Service13';
import { ClientRouter } from './protected/Routers/ClientRouter';


const app = express()

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded());
const foodService13 = new FoodService13()
const foodRouter12 = new FoodRouter12(foodService13)
const foodRouter13 = new FoodRouter13(foodService13)
const clientRouter = new ClientRouter()

app.use(express.static(__dirname + '/public'))


app.use('/foods12', foodRouter12.router()) // 開backend
app.use('/foods13', foodRouter13.router())
app.use('/client', clientRouter.router())






app.listen(8000)






  






