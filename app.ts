import "./passport"
import bodyParser = require('body-parser');
import * as jsonfile from "jsonfile";
import * as express from 'express'
import * as expressSession from 'express-session'
import * as passport from 'passport';
import {UserService} from './services/UserService';
import {isLoggedIn,loginFlow} from './guards';
import { UserRouter } from './Routers/UserRouter';/////////
import { FoodRouter12 } from './protected/Routers/Router12'
import { FoodRouter13 } from './protected/Routers/Router13'
import { FoodService13 } from './protected/Services/Service13';
import { ClientRouter } from './protected/Routers/ClientRouter';
import * as http from 'http';
import * as socketIO from 'socket.io';


const app = express()
const server = new http.Server(app);
const io = socketIO(server);

io.on('connection', function (socket) {
    
   socket.on('client-detail', async (msg) => {
    let forBoss ;
    try{
        // this.forBoss = jsonfile.readFileSync((`./purchase-for-boss.json`))
        // Non-blocking event loop
        forBoss = await jsonfile.readFile('./purchase-for-boss.json');
    } catch (e) {
        // Failed case, should fail immediately.
        // forBoss = []
        // send failed message
        return;
    }

    forBoss.forEach((o,i) => o.number = i+1)
    const boss = {}
    for (let order of forBoss){
        boss[order.number] = {detail: order.purchaseDetail, cost: order.purchaseTotalAfterCoupon, time: order.purchaseTime, totalCost: order.totalCost}
    }

    const bossLength = Object.keys(this.boss)
    // Better to remove console.log
    console.log('test')
    console.log(msg)


    
    console.log(this.boss)

    // Wrong , Should use spread operator
    msg = {
        ...msg = {
            purchaseDetail: msg.purchaseDetail,
            purchaseTotalAfterCoupon: msg.purchaseTotalAfterCoupon,
            purchaseTime: msg.purchaseTime,
            address: msg.address,
            number: bossLength.length
        }
    }
    io.emit('boss-client-detail', (msg))
    console.log(msg)
   
   
   })
});

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true,
    cookie:{secure:false}
}));


app.use(passport.initialize());
app.use(passport.session());


app.use(bodyParser.json())
app.use(bodyParser.urlencoded());

const foodService13 = new FoodService13()
const foodRouter12 = new FoodRouter12(foodService13)
const foodRouter13 = new FoodRouter13(foodService13)
const clientRouter = new ClientRouter()
export const userService = new UserService();
export const userRouter = new UserRouter(userService);/////////////
// app.get('/first',isLoggedIn,;
app.use(express.static(__dirname+'/public'));
// Never put any server side code inside express.static folder!!!!
app.use('/protected',isLoggedIn,express.static(__dirname+'/protected'));
// app.use('/apples',isLoggedIn,new AppleRouter(appleService).router());
app.use('/', userRouter.router())////////////
app.use('/foods12', isLoggedIn, foodRouter12.router()) // 開backend
app.use('/foods13', isLoggedIn, foodRouter13.router())
app.use('/client', isLoggedIn, clientRouter.router())




app.post('/login',
    (...rest)=>{
      console.log(12334)  
      passport.authenticate('local',loginFlow(...rest))(...rest);
    })
    // passport.authenticate('local',loginFlow(...rest))(...rest));





app.get('/logout',(req,res)=>{
        req.logout();
        res.redirect('/index.html')
    })






const PORT = 8080;
server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});


