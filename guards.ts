import {Request,Response,NextFunction} from 'express';
import {User} from './services/UserService';



export function isLoggedIn(req:Request,res:Response,next:NextFunction){
  if(req.user){
        next();
  }else{
        res.redirect('/index.html');
  }
}

// No Use
export function isAdminLoggedIn(req:Request,res:Response,next:NextFunction){
  if(req.user){
    next(); 
  }else{
      res.redirect('/index.html');
  }
}

export function loginFlow(req:Request,res:Response,next:NextFunction){
    return (err:Error,user:User,info:{message:string}) => {
      if(err){
        console.log("error.message")
        res.redirect("/login.html?error=" + err.message);

      }else if(info && info.message){
        console.log("info.message")
        res.redirect("/login.html?error=" + info.message);
      }else{
        req.logIn(user,(err) => {
          if(err){
            console.log("something wrong")
            res.redirect("/login.html?error="+"Failed to Login");
          }else{
            if (user.username == "admin"){
              res.redirect("/protected/boss.html");
            }else{

              console.log("can login")
              res.redirect("/protected/page12.html");
            }
          }
        });
      }
    };
}