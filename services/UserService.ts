
import * as jsonfile from "jsonfile";
import { hashPassword } from '../hash'
export interface User {
    id: number,
    username: string,
    password: string,

}



export class UserService {
    private users: User[]
    constructor() {
        try {
            // Use await 
            this.users = jsonfile.readFileSync(('./User.json'))
        } catch (e) {
            this.users = []
        }
    }

    getUsers() {
        return this.users;
    }

    async addUsers(username: string, password: string) {

        const char: any = "^[\\w-_\.]*[\\w]$"
        if (this.checkUserReg(username)) {
            throw new Error('呢個名有人改左啦，改過個第二個啦') // error message which goes to router then go to front end           
        } else if (username.trim() == "") {
            throw new Error('名都未打註咩冊呀？')
        } else if (!username.match(char)) {
            throw new Error('冇得用標點符號呀！')
        } else if (username.length < 4 || username.length > 16) {
            console.log(username.length)
            throw new Error('個名長D得唔得呀？')
        }else if (password.length > 3) {
            const hash = await hashPassword(password);
            this.users.push({ id: this.users.length + 1, username: username, password: hash })
            // Use {space:2} for auto-formatting
            await jsonfile.writeFile('./User.json', this.users,{ spaces: 2 });
        } else {
            throw new Error('密碼長D得唔＿得呀？')
        }
    }

    checkUserReg(username) {
        const findUser = this.users.find(user => user.username === username)
        return findUser
    }





}

