// No Use
import * as jsonfile from "jsonfile";
import * as path from 'path';
import {hashPassword} from '../hash'
import { userInfo } from 'os';
import { updateShorthandPropertyAssignment } from 'typescript';
export interface User{
    id:number,
    username:string,
    password:string,
    
}



export class UserService{
    private users:User[]
    constructor(){
        try{
            this.users = jsonfile.readFileSync(('./User.json'))
        }catch (e){
            this.users = []
        }
    }

    getUsers(){
        return this.users;
    }

    async addUsers(username:string,password:string){

        const char:any = "^[\\w-_\.]*[\\w]$"
       


        if (this.checkUserReg(username)){
            throw new Error('The username is existed.')
            
        }else if (username.length < 4 && username.length > 16){
            console.log(username.length)
            throw new Error('The username are not allowed to be lower than 4 chars.')
        }

        else{
            if(username.match(char)){

                const hash = await hashPassword(password);
                this.users.push({id: this.users.length + 1, username: username, password: hash})
            //    (id:this.users.length+1,username=${username},password=${hash})
                jsonfile.writeFileSync('./User.json', this.users)
            }else{
                throw new Error('The username are not allowed to be lower than 4 chars.')
            }
        
        

        
        }
    
            
    }

    checkUserReg(username){
        const findUser = this.users.find(user => user.username === username)        
        return findUser
    }
    


    

}

