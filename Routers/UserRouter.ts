import * as express from 'express';
// import {Request,Response} from 'express';
import {UserService} from '../services/UserService'
export class UserRouter{
    // private user = [];
    private userService:UserService
    

    constructor(userService:UserService){
        this.userService = userService
    }

    router(){
        const router = express.Router();
        router.post('/register',this.addUser);
        // router.get('/',this.getApples);
        // router.post('/',this.addApple);
        // router.put('/:id',this.editApple);
        // router.delete('/:id',this.deleteApple);
        return router;
    }

    
    addUser = async (req:express.Request,res:express.Response) => {  
                  
        try{
            await this.userService.addUsers(req.body.username,req.body.password);
            // Form Submit Good
            res.redirect('/success.html')
        }catch(e){
            // Should be another html to show the error
            // Not showing JSON
            // res.status(401).json({msg:e.message})
            res.redirect('/input.html?error='+e.message);
        }
        
    }
}

