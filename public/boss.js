const socket = io.connect()

document.querySelector('button').addEventListener('click', () => {
    socket.emit('msg', document.querySelector('input').value)
})

socket.on('got-msg', (msg) => {
    document.querySelector('#chatroom').innerHTML += `<div> ${msg} </div>`
})