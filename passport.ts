import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import {/*hashPassword,*/checkPassword} from './hash';
import {User} from './services/UserService';
import {userService} from './app';



const LocalStrategy = passportLocal.Strategy;
passport.use('local', new LocalStrategy(
    async function(username, password, done) {
        
      const users = userService.getUsers();
      const user = users.find((user) => user.username == username);
      const admin = users.find((user) => user.username == username[17])
      if(!user){
          return done(null,false,{message:'Incorrect username!'});
      }else{
          if(admin){ 
              return done(null,admin);

          } 
      }
      const match = await checkPassword(password,user.password);
      if(match){
          
          return done(null,user);
      }else{
          return done(null,false,{message:'Incorrect password!'});
      }
    }
  ));

passport.serializeUser(function(user:User, done) {
    done(null, user.id);
});

passport.deserializeUser(async function(id, done) {
    //由jsonfile讀返個USER出來
    const users = await userService.getUsers();
    const user = users.find((user)=> id == user.id);
    if(user){
        done(null,user);
    }else{
        done(new Error("User not Found"));
    }
});